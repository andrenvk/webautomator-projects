import json
import re
import os

import requests

api_url = 'https://www.bezrealitky.cz/api/record/markers'
params = {
    'offerType': 'pronajem',
    'estateType': 'byt',
    'priceTo': int(os.environ.get('price', 15000)),
    'hasDrawnBoundary': True,  # str(True).lower(),
    'boundary': json.dumps([[
        {"lat": 50.0823716114712, "lng": 14.46263150601817},
        {"lat": 50.07975541488113, "lng": 14.463489812902935},
        {"lat": 50.07634037505281, "lng": 14.464047712378033},
        {"lat": 50.073338243985475, "lng": 14.464433950476177},
        {"lat": 50.0706113748966, "lng": 14.4628889980836},
        {"lat": 50.06942692891014, "lng": 14.460013670019634},
        {"lat": 50.06846282337189, "lng": 14.456752103857525},
        {"lat": 50.0681873610869, "lng": 14.453662199072369},
        {"lat": 50.06807717572997, "lng": 14.450186056189068},
        {"lat": 50.06832509242713, "lng": 14.446838659338482},
        {"lat": 50.06887601383289, "lng": 14.44387750058604},
        {"lat": 50.06967483863026, "lng": 14.440487188391216},
        {"lat": 50.07074909927554, "lng": 14.437526029638775},
        {"lat": 50.07256702400949, "lng": 14.43490819364024},
        {"lat": 50.07537640850044, "lng": 14.433534902624615},
        {"lat": 50.07906691836426, "lng": 14.433363241247662},
        {"lat": 50.08096714473599, "lng": 14.43589524655772},
        {"lat": 50.08234407330321, "lng": 14.437783521704205},
        {"lat": 50.083170211459745, "lng": 14.442590040258892},
        {"lat": 50.08325282449234, "lng": 14.445851606421002},
        {"lat": 50.08330789976825, "lng": 14.450228971533306},
        {"lat": 50.0834180501303, "lng": 14.454005521826275},
        {"lat": 50.083252824492284, "lng": 14.457438749365338},
        {"lat": 50.082977447163344, "lng": 14.461000722937115},
        {"lat": 50.082729606214755, "lng": 14.462073606543072},
        {"lat": 50.0823716114712, "lng": 14.46263150601817},
    ]])
}

r = requests.post(
    api_url,
    params=params,
    headers={
        'User-Agent':
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0'
    }
)
data = r.json()
result  = {str(item['id']): item for item in data}
with open('./br.png', mode='w', encoding='utf-8') as f:
    json.dump(result, f, ensure_ascii=False, indent=4, sort_keys=True)
